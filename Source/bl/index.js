var bl =
{
    intervals: require('./intervals'),
    payments: require('./payments'),
    auth: require('./auth'),
    users: require('./users'),
    summary: require('./summary'),
    properties: require('./properties')
};

module.exports = bl;