/**
 * Root module of Summary BL functions.
 * Created by I.Denisovs on 16.3.7.
 */

var summary =
{
    payments: require('./get-payments-summary')
};

module.exports = summary;