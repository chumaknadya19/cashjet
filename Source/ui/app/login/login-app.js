/**
 * Created by I.Denisovs on 16.13.6.
 */
var deps = ['ui.bootstrap', 'ngCookies', 'ngResource', 'ngRoute', 'pascalprecht.translate'];

var app = angular.module('MoneySaverAppLogin', deps);

