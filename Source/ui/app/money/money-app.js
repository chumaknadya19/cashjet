var dependencies =
[
    'ngRoute', 'ngResource', 'ui.bootstrap', 'angularSpinner', 'ngCookies', 'chart.js',
    'ngSanitize', 'pascalprecht.translate'
];

var app = angular.module('MoneySaverApp', dependencies);