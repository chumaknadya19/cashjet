var timezones = {
	getAll: require('./get-all'),
	getById: require('./get-by-id')
};

module.exports = timezones;