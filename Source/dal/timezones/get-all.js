var timezones = require('./timezones.json');

function getAllTimezones() {
	return timezones;
}

module.exports = getAllTimezones;